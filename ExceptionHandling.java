import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false; //untuk mengecek apakah input sudah valid atau belum
        do {
            //edit here
            // Membuat try-catch untuk menangkap exception yang mungkin terjadi
            try {  
              System.out.print("Masukkan nilai pembilang : ");
              int pembilang = scanner.nextInt();

              System.out.print("Masukkan nilai penyebut : ");
              int penyebut = scanner.nextInt();

              int hasil = pembagian(pembilang, penyebut);
              System.out.print("hasil pembagian : " +hasil);

              validInput = true; // Mengubah nilai variabel validInput menjadi true karena input sudah valid
            } catch(InputMismatchException e){ // Menangani exception jika input bukan bilangan bulat
                System.out.println("Input tidak valid, masukkan bilangan bulat");
                scanner.nextLine();
            } 
            catch(ArithmeticException e){ //// Menangani exception jika input bukan bilangan bulat
                System.out.println(e.getMessage()); // Menampilkan pesan yang ada di dalam exception, yaitu "Penyebut tidak boleh bernilai 0"
            }
        } while (!validInput); // Mengulangi perulangan selama validInput masih false

        scanner.close(); // Menutup objek scanner
    }

    public static int pembagian(int pembilang, int penyebut) {
        //add exception apabila penyebut bernilai 0
        if (penyebut == 0){
            // Melempar exception ArithmeticException dengan pesan "Penyebut tidak boleh bernilai 0"
            throw new ArithmeticException("Penyebut tidak boleh bernilai 0");
        }
        return pembilang / penyebut; // Mengembalikan hasil pembagian antara pembilang dan penyebut
    }
}
